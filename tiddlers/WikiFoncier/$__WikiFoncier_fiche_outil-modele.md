_La fiche modèle présente un plan type avec une série de questions pouvant servir base à la rédaction. Les questions sont là pour aider la construction de la fiche, elle ne sont pas toutes obligatoires. L'objectif doit être de rester suffisamment concis, quitte à renvoyer à d'autres (sous-)fiches si nécessaire_.

**Paragraphe d'introduction** décrivant sommairement l'outil, ou la famille d'outils (_Dans quel(s) cas utilise-t-on cet outil? dans le cas particulier de l'étude ZAE, des indications sur le scénario sont intéressantes par exemple_)

## Pilotage et partenariats
_Qui peut utiliser cet outil? Qui pilote sa mise en place? Avec quels partenaires?_

## Mise en oeuvre
_Quelle est la procédure à suivre? Auprès de qui ? Que faire ? Dans quel ordre ? Quels éléments produire ? Qui peut aider ? Quel prestataire peut être sollicité ? Combien de temps cela prend-il ?_

## Recommandations
_Quel avantage présente l'outil? Quels inconvénients? Quels sont les points de vigilance? Les points faibles? Les bonnes pratiques à adopter? Que faut-il éviter? Avec quel autre outil se combine-t-il bien? ou mal?_

## Références et guides
_C'est un peu la partie : "je suis intéressé, que dois-je lire en premier avant d'y aller".
A quelles références réglementaires se référer? Quels guides et méthodes sont disponibles? Y a-t-il des exemples existant?_