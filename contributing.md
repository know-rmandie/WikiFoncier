[WikiFoncier](#WikiFoncier) se veut une initiative ouverte et évolutive. A ce titre, chacun est invité à participer à son amélioration. Dans tous les cas, n'hésitez pas à nous contacter, le petit guide qui suit ne pouvant couvrir toutes les possibilités de participation possible.

## Faire des remarques
Il est possible de faire parvenir des remarques et commentaires :

* Par mail à l'adresse : `badd.seclad.dreal-normandie (chez) developpement-durable.gouv.fr`
* Directement sur le site du wiki : [https://framagit.org/know-rmandie/WikiFoncier/issues](https://framagit.org/know-rmandie/WikiFoncier/issues)

## Proposer des modifications plus complètes
Ces modifications peuvent comprendre des modifications en profondeur des fiches existantes ou même de nouvelles fiches.

#### Envoi des fiches modifiées
Il est possible de faire parvenir des fiches nouvelles ou modifiées au format .tid (tiddlywiki) ou .md (markdown) par les deux canaux précédemment cités.

#### Propositions sur le dépôt du WikiFoncier
Il est également possible de proposer directement les modifications effectuées via une _**merge-request**_ sur le [dépôt du projet][repo].

* Clonez le projet sur Framagit (utilisez _**fork**_)
* Récupérez les sources sur votre environnement de travail
* Basez vous sur la branche `wip`
* Effectuez vos modifications / corrections
* Envoyez les modifications sur votre dépôt Framagit
* Proposez l'intégration de vos modifications depuis votre dépôt Framagit (//merge request//)

#### Recommandations

* Le cas échéant (si du temps a passé par exemple) pensez à rebaser votre travail sur la dernière version de la branche `wip`, pour traiter vous mêmes les conflits éventuels
* Proposez des ensembles de modifications ne comportant pas trop de changements à la fois (et qui soient dans tous les cas cohérents), cela facilite la revue avant fusion et donc facilite l'intégration
* Travaillez sur votre propre branche plutôt que sur la branche `wip` afin de proposer un commit unique comportant toutes les modifications
* Décrivez de manière complète les modifications et corrections proposées dans le commit
* Référez-vous si nécessaire au [gestionnaire d'issues du projet][issues] pour étiqueter votre commit (`#numero d'isssue`)

## Environnment de travail recommandé
Il y a de nombreuses façon de travailler sur WikiFoncier et il n'y a pas de _bonne_ ou de _mauvaise_ façon de travailler. L'utilisation d'outils plus ou moins poussés dépend de l'ampleur de votre contribution.

#### Pour des corrections ou propositions minimes

L'utilisation du [gestionnaire d'issues du projet][issues] ou de la boite mail mentionnée plus haut suffira largement. L'avantage du gestionnaire d'issue est de permettre le suivi de votre proposition dans le temps et d'instaurer un dialogue transparent avec tous les participants qui le souhaitent. La création d'un compte sur Framagit est toutefois nécessaire.

#### Pour des corrections plus complètes ou la proposition de fiches nouvelles

L'outil le plus souple est de récupérer le Wiki autonome via la page [télécharger][downloadWiki]. Vous pourrez ensuite travailler sur ce wiki depuis
   * votre navigateur web (les dernières versions de Firefox et Chrome conviennent) en leur ajoutant éventuellement une extension pour faciliter le travail (voir la [documentation de TiddlyWiki][twdoc] à ce sujet)
   * le logiciel [TiddlyDesktop][tiddlydesktop]

#### Pour une association plus étroite au projets

Il est recommandé de disposer d'un [client git][gitclients]. Notre équipe utilise TortoiseGit sous windows et gitKraken pour un suivi plus visuel de l'évolution du projet.

Il est également recommandé de travailler avec la version Node.js de TiddlyWiki :

  * Installer [Node][nodejs]
  * Installer TiddlyWiki depuis un terminal : `npm install -g tiddlywiki`
  * Se placer dans le répertoire de WikiFoncier (cloné via git depuis votre dépôt), basculer dans sa branche de travail et lancer le wiki depuis un terminal : `tiddlywiki ./ --server`
  * Se rendre dans le navigateur web à l'adresse http://127.0.0.1:8080
  * Quand les modifications sont faites il est possible de tester le résultat du wiki "statique" via `tiddlywiki ./ --build` (les fichiers sont dans le sous-repertoire `output`)

Si pour une raison particulière l'installation de Node ou TiddlyWiki n'est pas possible, l'usage de [TiddlyDesktop][tiddlydesktop] est possible également pour travailler sur le répertoire de WikiFoncier. Seul bémol, on ne pourra pas tester l'export dans le wiki statique.   

[repo]: https://framagit.org/know-rmandie/WikiFoncier
[issues]: https://framagit.org/know-rmandie/WikiFoncier/issues
[downloadWiki]: https://know-rmandie.frama.io/WikiFoncier/telecharger.html
[twdoc]: https://tiddlywiki.com/languages/fr-FR/index.html#GettingStarted
[tiddlydesktop]: https://github.com/Jermolene/TiddlyDesktop/releases
[gitclients]: https://git-scm.com/downloads/guis
[nodejs]: https://nodejs.org/fr/
